/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  TouchableHighlight,
  Alert,
  StyleSheet,
  Animated,
} from 'react-native';

import I18n from '../../services/translation';
import getRealm from '../../services/realm';
//  LIMPIAR Y AUTOMATIZAR TODO!!!!!!!!
// TODO!!!! LAS FUENTES EN IOS Y FUENTES E ICONS EN ANDROID ARREGLAR LO SE ORDENAR POR TIEMPO
//'¡¡TODOOO!! si agregar lo de traer los tasks del dia actual por que a lo mejor alguien no quiere borrar tasks expirados entonces si nesecito eso
import Swipeable from 'react-native-swipeable';
import SwitchSelector from 'react-native-switch-selector';
import Modal from 'react-native-modal';
// import {CountdownCircleTimer} from 'react-native-countdown-circle-timer';

import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';

import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';

import DoneTaskSound from '../../assets/audio/notification_test.mp3';

import BottomModal from '../BottomModal';
import CreateEditTask from '../BottomModal/CreateEditContent';

import {useTheme, useNavigation} from '@react-navigation/native';

import LottieView from 'lottie-react-native';
import Done from '../../assets/animations/done.json';

import SettingsOptionsContext from '../../contexts/SettingsOptionsContext';

import {
  responsive,
  handleSound,
  storeSettingsData,
  getSettingsData,
  removeSettingsData,
  handleReadableDate,
  truncate,
  tasksSortSelector,
  sortOrder,
  handleNotification,
  handleFuturePushNotification,
} from '../../utils';

const size = responsive();

const Task = (props) => {
  // useEffect(() => {
  //   const timer = BackgroundTimer.setTimeout(() => {
  //     BackgroundTimer.setInterval(() => {
  //       console.log('cce');
  //     }, 3000);
  //   }, 5000);

  // BackgroundTimer.clearTimeout(timer);
  // }, []);
  const {colors} = useTheme();
  const navigation = useNavigation();

  const [userTasks, setUserTasks] = useState([]);

  const [taskToUpdate, setTaskToUpdate] = useState('');

  const [userSubtasks, setUserSubtasks] = useState([]);

  //MODAL CREATE & UPDATE TASK STATES
  const [inputNameTask, setInputNameTask] = useState('');
  const [selectedColor, setSelectedColor] = useState('#2ED27C');
  // const [alarm, setAlarm] = useState(false);
  const [selectedIcon, setSelectedIcon] = useState('');

  //STATES CON VALOR DE LA HORA EN LA QUE SONARA EL TASK Y LA MOSTRARAN POR DEFECTO EN EL DATETIMEPICKER
  const [taskHour, setTaskHour] = useState(0);
  const [taskMinute, setTaskMinute] = useState(0);

  const [selectedSort, setSelectorSort] = useState();

  // const [pendingAlarmsArr, setPendingAlarmsArr] = useState([]);
  // const [turnOnAlarm, setTurnOnAlarm] = useState(true);
  // const [turnOffAlarm, setTurnOffAlarm] = useState(false);

  const [isMenuModalVisible, setMenuModalVisible] = useState(false);
  const [doneTask, setDoneTask] = useState(false);

  const [modalDoneTaskVisible, setModalDoneTaskVisible] = useState(false);

  const [shotAnimation, setShotAnimation] = useState(false);

  const createTaskrefBottomModalTEST = useRef();
  const editTaskrefBottomModalTEST = useRef();
  const taskOrRoutineBottomModalTEST = useRef();

  const tasksOpacity = useRef(new Animated.Value(0)).current;

  const {deleteExpired, soundDone} = useContext(SettingsOptionsContext);

  getSettingsData('sortSelected', (value) => {
    setSelectorSort(value);
  });

  const handleAnimation = () => {
    setShotAnimation(true);
    handleSound(DoneTaskSound);
  };

  // useEffect(() => {
  //   getSettingsData('pendingAlarms', async (value) => {
  //     if (value) {
  //       const myArr = JSON.parse(value);
  //       myArr.map((item) => console.log('ARRSTRING', item));

  //       const realm = await getRealm();

  //       const foundValue = realm.objectForPrimaryKey('Task', myArr[0]);
  //       console.log(foundValue);

  //       const currentDate = new Date();

  //       if (
  //         foundValue.soundHour === currentDate.getHours() &&
  //         foundValue.soundMinute === currentDate.getMinutes()
  //       ) {
  //         Alert.alert('es hoy es hoy2');
  //         setTurnOnAlarm(true);
  //         navigation.navigate('turnOffAlarm');
  //       } else {
  //         Alert.alert('nel hoy no2');
  //       }
  //     } else {
  //       console.log('no hay ALARMAS');
  //     }
  //   });
  // }, [navigation]);

  const handleCreateTaskView = () => {
    const currentDate = new Date();
    return (
      <View>
        {(deleteExpired && currentDate.getDate() > props.day) ||
        currentDate.getMonth() > props.month ? (
          <View style={styles.conatiner}>
            <Text
              style={{
                ...styles.questionTxt,
                color: colors.text,
                fontSize: 20,
              }}>
              Aun no se puede viajar en el tiempo 😓
            </Text>
          </View>
        ) : (
          <View style={styles.conatiner}>
            <Text style={{...styles.questionTxt, color: colors.text}}>
              {I18n.t('question')}
            </Text>
            <Text style={{...styles.addTxt, color: colors.text}}>
              {I18n.t('add')}
            </Text>
            <TouchableOpacity
              onPress={() => {
                (deleteExpired && currentDate.getDate() > props.day) ||
                currentDate.getMonth() > props.month
                  ? Alert.alert('Aun no podemos viajar en el tiempo :(')
                  : taskOrRoutineBottomModalTEST.current.open();
              }}>
              <AntDesign name="pluscircle" size={49} color="#59EEFF" />
            </TouchableOpacity>
            {taskOrRoutineModal()}
          </View>
        )}
      </View>
    );
  };

  const handleCreateAndSeveNewTask = async (t, c, hr, mn, i, subtArr) => {
    const data = {
      id: uuidv4(),
      name: t,
      color: c,
      done: false,
      icon: i,
      soundYear: props.year,
      soundMonth: props.month,
      soundDay: props.day,
      soundHour: hr,
      soundMinute: mn,
    };

    const realm = await getRealm();

    try {
      realm.write(() => {
        let newTask = realm.create('Task', data);
        subtArr.map((item) => newTask.subtasks.push(item));
      });
    } catch (error) {
      console.log('ERR', error);
    }

    setUserTasks(
      realm
        .objects('Task')
        .filtered(
          `soundDay == ${props.day} AND soundMonth == ${props.month} AND soundYear == ${props.year}`,
        ),
    );

    // const pendingAlarmsData = realm.objects('Task').filtered('alarm == true');

    // const pendingAlarmsDataIds = pendingAlarmsData.map((itemId) => itemId.id);

    // if (pendingAlarmsDataIds.length > 0) {
    //   storeSettingsData('pendingAlarms', JSON.stringify(pendingAlarmsDataIds));
    // } else {
    //   removeSettingsData('pendingAlarms');
    // }

    // setPendingAlarmsArr(pendingAlarmsDataIds);
    createTaskrefBottomModalTEST.current.close();
  };

  const handleUpdateAndSaveTask = async (t, c, hr, mn, i, subtArr) => {
    const realm = await getRealm();

    try {
      realm.write(() => {
        let foundTaskToUpdate = realm.create(
          'Task',
          {
            id: taskToUpdate,
            name: t,
            color: c,
            done: false,
            icon: i,
            soundYear: props.year,
            soundMonth: props.month,
            soundDay: props.day,
            soundHour: hr,
            soundMinute: mn,
          },
          'modified',
        );
        subtArr.map((item) => foundTaskToUpdate.subtasks.push(item));

        const data = realm
          .objects('Task')
          .filtered(
            `soundDay == ${props.day} AND soundMonth == ${props.month} AND soundYear == ${props.year}`,
          );

        setUserTasks(data);

        // const pendingAlarmsData = realm
        //   .objects('Task')
        //   .filtered('alarm == true');

        // const pendingAlarmsDataIds = pendingAlarmsData.map(
        //   (itemId) => itemId.id,
        // );

        // if (pendingAlarmsDataIds.length > 0) {
        //   storeSettingsData(
        //     'pendingAlarms',
        //     JSON.stringify(pendingAlarmsDataIds),
        //   );
        // } else {
        //   removeSettingsData('pendingAlarms');
        // }

        // setPendingAlarmsArr(pendingAlarmsDataIds);
        editTaskrefBottomModalTEST.current.close();
      });
    } catch (error) {
      console.log('ERR', error);
    }
  };

  const taskOrRoutineModal = () => {
    let paddingVerticalContainer;
    let paddingHorizontalPlusIconContainer;
    let icons;
    let fontSize;
    if (size === 'small') {
      paddingVerticalContainer = 15;
      paddingHorizontalPlusIconContainer = 20;
      icons = 35;
      fontSize = 10;
    } else if (size === 'medium') {
      paddingVerticalContainer = 22;
      paddingHorizontalPlusIconContainer = 28;
      icons = 45;
      fontSize = 12;
    } else {
      //large screen
      paddingVerticalContainer = 25;
      paddingHorizontalPlusIconContainer = 30;
      icons = 55;
      fontSize = 15;
    }

    return (
      <BottomModal
        openModal={taskOrRoutineBottomModalTEST}
        wrapperColor={colors.modalWrapper}
        muchContent={false}
        borderRadiusTop={40}
        closeDragDown={true}
        content={
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              alignItems: 'center',
              height: '90%',
            }}>
            <TouchableOpacity
              onPress={() => createTaskrefBottomModalTEST.current.open()}>
              <View
                style={{
                  backgroundColor: colors.forms,
                  paddingVertical: paddingVerticalContainer,
                  paddingHorizontal: paddingHorizontalPlusIconContainer,
                  alignItems: 'center',
                  borderRadius: 20,
                }}>
                <AntDesign name="plus" color={colors.text} size={icons} />
                <Text style={{color: colors.text, fontSize: fontSize}}>
                  {I18n.t('newTask')}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View
                style={{
                  backgroundColor: colors.forms,
                  padding: paddingVerticalContainer,
                  alignItems: 'center',
                  borderRadius: 20,
                }}>
                <AntDesign name="bars" color={colors.text} size={icons} />
                <Text style={{color: colors.text, fontSize: fontSize}}>
                  {I18n.t('addRoutine')}
                </Text>
              </View>
            </TouchableOpacity>
            {createTaskModal()}
          </View>
        }
      />
    );
  };

  const createTaskModal = () => {
    return (
      <BottomModal
        openModal={createTaskrefBottomModalTEST}
        wrapperColor={colors.subModalWrapper}
        muchContent={true}
        borderRadiusTop={40}
        keyBoardPushContent={false}
        closeDragDown={true}
        content={
          <CreateEditTask
            modalTitle={I18n.t('new')}
            buttonSubmitText={I18n.t('create')}
            placeHolder={I18n.t('title')}
            passAllData={(txt, color, h, m, icn, subtasksArr) => {
              handleCreateAndSeveNewTask(txt, color, h, m, icn, subtasksArr);
            }}
            editModal={false}
          />
        }
      />
    );
  };

  const editTaskModalTEST = () => {
    return (
      <BottomModal
        openModal={editTaskrefBottomModalTEST}
        wrapperColor={colors.modalWrapper}
        muchContent={true}
        borderRadiusTop={40}
        keyBoardPushContent={false}
        closeDragDown={true}
        content={
          <CreateEditTask
            modalTitle={I18n.t('editTask')}
            buttonSubmitText={I18n.t('update')}
            placeHolder={I18n.t('title')}
            passAllData={(txt, color, h, m, icn, subtasksArr) =>
              handleUpdateAndSaveTask(txt, color, h, m, icn, subtasksArr)
            }
            editModal={true}
            currentTaskName={inputNameTask}
            currentTaskColor={selectedColor}
            // currentTaskAlarmOrNotification={alarm}
            currentTaskYear={props.year}
            currentTaskMonth={props.month}
            currentTaskDay={props.day}
            currentTaskHour={taskHour}
            currentTaskMinute={taskMinute}
            currentTaskIcon={selectedIcon}
            currentSubtasks={userSubtasks}
          />
        }
      />
    );
  };

  // const handleDoneTaskModal = () => {
  //   return (
  //   );
  // };

  useEffect(() => {
    const handleShowTasks = async () => {
      const realm = await getRealm();
      console.log('AQIIIII', realm.path);
      const data = realm
        .objects('Task')
        .filtered(
          `soundDay == ${props.day} AND soundMonth == ${props.month} AND soundYear == ${props.year}`,
        );

      setUserTasks(data);

      // const pendingAlarmsData = realm.objects('Task').filtered('alarm == true');

      // const pendingAlarmsDataIds = pendingAlarmsData.map((itemId) => itemId.id);

      // if (pendingAlarmsDataIds.length > 0) {
      //   storeSettingsData(
      //     'pendingAlarms',
      //     JSON.stringify(pendingAlarmsDataIds),
      //   );
      // } else {
      //   removeSettingsData('pendingAlarms');
      // }

      // setPendingAlarmsArr(pendingAlarmsDataIds);
    };
    handleShowTasks();

    Animated.sequence([
      Animated.timing(tasksOpacity, {
        toValue: 0,
        duration: 0,
      }),
      Animated.timing(tasksOpacity, {
        toValue: 1,
        duration: 1300,
      }),
    ]).start();
  }, [props.day, props.month, props.year, tasksOpacity]);

  const handleDeleteExpiredTasks = async () => {
    const realm = await getRealm();

    const currentDay = new Date().getDate();
    const currentMonth = new Date().getMonth();

    const expiredTasks = realm
      .objects('Task')
      .filtered(`soundDay < ${currentDay} || soundMonth < ${currentMonth}`);

    expiredTasks.length > 0
      ? realm.write(() => {
          realm.delete(expiredTasks);
        })
      : console.log('no hay');
  };

  useEffect(() => {
    console.log('D EXP T:', deleteExpired);
    deleteExpired ? handleDeleteExpiredTasks() : null;
  }, [deleteExpired, soundDone]);

  // getSettingsData('pendingAlarms', () => {});

  useEffect(() => {
    handleFuturePushNotification('hola', 'bro', 2021, 2, 15, 19, 9);
  }, []);

  const handleShowTasksView = () => {
    let paddingBottomFlatlist;

    let todayTextSize;

    let plusMenuIconsContainerWidth;
    let plusMenuIconsSize;
    let iconTaskSize;
    let nameTaskSize;
    let iconListTaskSize;
    let modeTaskIconSize;
    let timeTaskSize;

    let paddingHorizontalTask;
    let paddingVerticalTask;

    if (size === 'small') {
      paddingBottomFlatlist = '77%';
      todayTextSize = 12;
      plusMenuIconsSize = 20;
      plusMenuIconsContainerWidth = '21%';

      paddingHorizontalTask = 35;
      paddingVerticalTask = 10;
      iconTaskSize = 35;
      nameTaskSize = 11;
      iconListTaskSize = 20;
      modeTaskIconSize = 12;
      timeTaskSize = 11;
    } else if (size === 'medium') {
      paddingBottomFlatlist = '81%';

      todayTextSize = 14;
      plusMenuIconsSize = 27;
      plusMenuIconsContainerWidth = '22%';

      paddingHorizontalTask = 37;
      paddingVerticalTask = 14;
      iconTaskSize = 47;
      nameTaskSize = 13;
      iconListTaskSize = 22;
      modeTaskIconSize = 14;
      timeTaskSize = 13;
    } else {
      paddingBottomFlatlist = '84%';

      todayTextSize = 18;
      plusMenuIconsSize = 32;
      plusMenuIconsContainerWidth = '23%';

      paddingHorizontalTask = 40;
      paddingVerticalTask = 16;
      iconTaskSize = 54;
      nameTaskSize = 15;
      iconListTaskSize = 26;
      modeTaskIconSize = 16;
      timeTaskSize = 15;
    }

    return (
      <View style={styles.conatiner}>
        <View style={styles.conatiner15}>
          <View style={styles.conatiner2}>
            <View
              style={{
                flexDirection: 'row',
                width: '20%',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  ...styles.dayWeek,
                  color: colors.text,
                  fontSize: todayTextSize,
                }}>
                {I18n.t('today')}
              </Text>
              <View style={{}}>
                <Text
                  style={{
                    color: '#6925F8',
                    fontWeight: 'bold',
                    fontSize: 15,
                  }}>
                  {userTasks.length}
                </Text>
              </View>
              {shotAnimation ? (
                <LottieView
                  style={{
                    width: 100,
                    height: 100,
                    left: 40,
                    position: 'absolute',
                  }}
                  source={Done}
                  autoPlay={true}
                  onAnimationFinish={() => setShotAnimation(false)}
                  loop={false}
                  speed={2.5}
                />
              ) : null}
            </View>
            <View
              style={{
                ...styles.conatiner3,
                width: plusMenuIconsContainerWidth,
              }}>
              <TouchableOpacity onPress={() => setMenuModalVisible(true)}>
                <Ionicons
                  name="ios-ellipsis-horizontal-circle-outline"
                  color={colors.text}
                  size={plusMenuIconsSize}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  createTaskrefBottomModalTEST.current.open();
                }}>
                <AntDesign
                  name="pluscircle"
                  size={plusMenuIconsSize}
                  color="#59EEFF"
                />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{flex: 1}}>
            {selectedSort === '1' ? (
              <FlatList
                data={userTasks
                  .map((item) => item)
                  .sort(
                    (a, b) =>
                      sortOrder.indexOf(a.color) - sortOrder.indexOf(b.color) ||
                      a.soundHour - b.soundHour ||
                      a.soundMinute - b.soundMinute,
                  )}
                keyExtractor={(item) => item.id}
                numColumns={1}
                style={{
                  paddingBottom: paddingBottomFlatlist,
                  // backgroundColor: 'gray',
                }}
                scrollEnabled={true}
                renderItem={({item}) => (
                  <Swipeable
                    leftActionActivationDistance={150}
                    onLeftActionRelease={async () => {
                      const realm = await getRealm();

                      if (item.done === false) {
                        setModalDoneTaskVisible(true);
                      }

                      setDoneTask(!doneTask);
                      realm.write(() => {
                        realm.create(
                          'Task',
                          {id: item.id, done: !item.done},
                          'modified',
                        );
                      });
                      const data = realm
                        .objects('Task')
                        .filtered(
                          `soundDay == ${props.day} AND soundMonth == ${props.month} AND soundYear == ${props.year}`,
                        );

                      setUserTasks(data);
                    }}
                    leftContent={
                      <TouchableHighlight
                        style={{
                          backgroundColor: item.color,
                          paddingVertical: paddingVerticalTask,
                          paddingHorizontal: paddingHorizontalTask,
                          borderRadius: 190,
                          marginTop: 20,
                          // marginRight: 15,
                          alignItems: 'flex-end',
                        }}>
                        <View
                          style={{
                            flexDirection: 'column',
                            alignItems: 'center',
                          }}>
                          <Ionicons
                            name="ios-checkmark-circle"
                            color="white"
                            size={iconTaskSize - 3}
                          />
                        </View>
                      </TouchableHighlight>
                    }
                    rightActionActivationDistance={150}
                    onRightActionRelease={() =>
                      Alert.alert(
                        I18n.t('deleteTask'),
                        'Deseas eliminar la tarea permanentemente',
                        [
                          {
                            text: 'Eliminar',
                            onPress: async () => {
                              const realm = await getRealm();
                              realm.write(() => {
                                const foundTask = realm.objectForPrimaryKey(
                                  'Task',
                                  item.id,
                                );
                                realm.delete(foundTask.subtasks);
                                realm.delete(foundTask);
                              });
                              const data = realm
                                .objects('Task')
                                .filtered(
                                  `soundDay == ${props.day} AND soundMonth == ${props.month} AND soundYear == ${props.year}`,
                                );

                              setUserTasks(data);
                            },
                          },
                          {
                            text: 'Cancelar',
                            onPress: () => console.log('cancelado'),
                          },
                        ],
                      )
                    }
                    rightContent={
                      <TouchableHighlight
                        style={{
                          backgroundColor: '#FE354B',
                          paddingVertical: paddingVerticalTask,
                          paddingHorizontal: paddingHorizontalTask,
                          borderRadius: 190,
                          marginTop: 20,
                          // marginLeft: 1,
                        }}>
                        <MaterialCommunityIcons
                          name="delete-circle"
                          color="white"
                          size={iconTaskSize}
                        />
                      </TouchableHighlight>
                    }>
                    <TouchableOpacity
                      onPress={() => {
                        setTaskToUpdate(item.id);
                        setInputNameTask(item.name);
                        setSelectedColor(item.color);
                        // setAlarm(item.alarm);
                        setTaskHour(item.soundHour);
                        setTaskMinute(item.soundMinute);
                        setSelectedIcon(item.icon);
                        setUserSubtasks(item.subtasks);
                        editTaskrefBottomModalTEST.current.open();
                      }}>
                      <View
                        style={{
                          backgroundColor: item.done ? '#EDEBEA' : item.color,
                          paddingVertical: paddingVerticalTask,
                          paddingHorizontal: paddingHorizontalTask,
                          borderRadius: 190,
                          marginTop: 20,
                          marginBottom: 10,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          width: '87%',
                          alignSelf: 'center',
                          alignItems: 'center',
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                          }}>
                          <MaterialCommunityIcons
                            name={item.icon}
                            size={iconTaskSize}
                            color="white"
                          />
                          <View
                            style={{
                              flexDirection: 'column',
                              justifyContent: 'center',
                              marginLeft: 10,
                            }}>
                            <Text
                              style={{
                                ...styles.dayWeek,
                                fontSize: nameTaskSize,
                              }}>
                              {item.subtasks.length > 0
                                ? truncate(item.name, 22)
                                : truncate(item.name, 30)}
                            </Text>
                            <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginTop: 2,
                              }}>
                              <Ionicons
                                name="notifications"
                                color="white"
                                size={modeTaskIconSize}
                              />
                              <Text
                                style={{
                                  ...styles.dayWeek,
                                  fontSize: timeTaskSize,
                                }}>
                                {handleReadableDate(
                                  item.soundHour,
                                  item.soundMinute,
                                )}
                              </Text>
                            </View>
                          </View>
                        </View>
                        {item.subtasks.length > 0 ? (
                          <Entypo
                            name="flow-cascade"
                            color="white"
                            size={iconListTaskSize}
                          />
                        ) : (
                          <Text />
                        )}
                      </View>
                    </TouchableOpacity>
                  </Swipeable>
                )}
              />
            ) : (
              <Animated.FlatList
                data={userTasks
                  .map((item) => item)
                  .sort(
                    (a, b) =>
                      a.soundHour - b.soundHour ||
                      a.soundMinute - b.soundMinute,
                  )}
                keyExtractor={(item) => item.id}
                numColumns={1}
                style={{
                  paddingBottom: paddingBottomFlatlist,
                  // backgroundColor: 'gray',
                  opacity: tasksOpacity,
                }}
                scrollEnabled={true}
                renderItem={({item}) =>
                  item.pomodoro ? (
                    <Swipeable
                      leftButtonWidth={95}
                      leftButtons={[
                        <TouchableOpacity
                          style={{
                            backgroundColor: '#0B6DF6',
                            paddingVertical: paddingVerticalTask,
                            paddingHorizontal: 20,
                            borderRadius: 190,
                            marginTop: 20,
                            // marginRight: 15,
                            alignItems: 'flex-end',
                          }}>
                          <View
                            style={{
                              flexDirection: 'column',
                              alignItems: 'center',
                            }}>
                            <Ionicons
                              name="ios-checkmark-circle"
                              color="white"
                              size={iconTaskSize - 3}
                            />
                          </View>
                        </TouchableOpacity>,
                        <TouchableOpacity
                          style={{
                            backgroundColor: 'lightblue',
                            paddingVertical: paddingVerticalTask,
                            paddingHorizontal: 20,
                            borderRadius: 190,
                            marginTop: 20,
                            // marginRight: 15,
                            alignItems: 'flex-end',
                          }}>
                          <View
                            style={{
                              flexDirection: 'column',
                              alignItems: 'center',
                            }}>
                            <MaterialCommunityIcons
                              name="progress-clock"
                              color="white"
                              size={iconTaskSize + 1}
                            />
                          </View>
                        </TouchableOpacity>,
                      ]}
                      rightActionActivationDistance={150}
                      onRightActionRelease={() =>
                        Alert.alert(
                          I18n.t('deleteTask'),
                          'Deseas eliminar la tarea permanentemente',
                          [
                            {
                              text: 'Eliminar',
                              onPress: async () => {
                                const realm = await getRealm();
                                realm.write(() => {
                                  const foundTask = realm.objectForPrimaryKey(
                                    'Task',
                                    item.id,
                                  );
                                  realm.delete(foundTask.subtasks);
                                  realm.delete(foundTask);
                                });
                                const data = realm
                                  .objects('Task')
                                  .filtered(
                                    `soundDay == ${props.day} AND soundMonth == ${props.month} AND soundYear == ${props.year}`,
                                  );

                                setUserTasks(data);
                              },
                            },
                            {
                              text: 'Cancelar',
                              onPress: () => console.log('cancelado'),
                            },
                          ],
                        )
                      }
                      rightContent={
                        <TouchableHighlight
                          style={{
                            backgroundColor: '#FE354B',
                            paddingVertical: paddingVerticalTask,
                            paddingHorizontal: paddingHorizontalTask,
                            borderRadius: 190,
                            marginTop: 20,
                            // marginLeft: 1,
                          }}>
                          <MaterialCommunityIcons
                            name="delete-circle"
                            color="white"
                            size={iconTaskSize}
                          />
                        </TouchableHighlight>
                      }>
                      <TouchableOpacity
                        onPress={() => {
                          setTaskToUpdate(item.id);
                          setInputNameTask(item.name);
                          setSelectedColor(item.color);
                          // setAlarm(item.alarm);
                          setTaskHour(item.soundHour);
                          setTaskMinute(item.soundMinute);
                          setSelectedIcon(item.icon);
                          setUserSubtasks(item.subtasks);
                          editTaskrefBottomModalTEST.current.open();
                        }}>
                        <View
                          style={{
                            backgroundColor: item.done ? '#EDEBEA' : item.color,
                            paddingVertical: paddingVerticalTask,
                            paddingHorizontal: paddingHorizontalTask,
                            borderRadius: 190,
                            marginTop: 20,
                            marginBottom: 10,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            width: '87%',
                            alignSelf: 'center',
                            alignItems: 'center',
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                            }}>
                            <MaterialCommunityIcons
                              name={item.icon}
                              size={iconTaskSize}
                              color="white"
                            />
                            <View
                              style={{
                                flexDirection: 'column',
                                justifyContent: 'center',
                                marginLeft: 10,
                              }}>
                              <Text
                                style={{
                                  ...styles.dayWeek,
                                  fontSize: nameTaskSize,
                                }}>
                                {item.subtasks.length > 0
                                  ? truncate(item.name, 22)
                                  : truncate(item.name, 30)}
                              </Text>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                  marginTop: 2,
                                }}>
                                <Ionicons
                                  name="notifications"
                                  color="white"
                                  size={modeTaskIconSize}
                                />
                                <Text
                                  style={{
                                    ...styles.dayWeek,
                                    fontSize: timeTaskSize,
                                  }}>
                                  {handleReadableDate(
                                    item.soundHour,
                                    item.soundMinute,
                                  )}
                                </Text>
                              </View>
                            </View>
                          </View>
                          {item.subtasks.length > 0 ? (
                            <Entypo
                              name="flow-cascade"
                              color="white"
                              size={iconListTaskSize}
                            />
                          ) : (
                            <Text />
                          )}
                        </View>
                      </TouchableOpacity>
                    </Swipeable>
                  ) : (
                    <Swipeable
                      leftActionActivationDistance={150}
                      onLeftActionRelease={async () => {
                        item.done === false && soundDone
                          ? handleAnimation()
                          : null;
                        const realm = await getRealm();

                        if (item.done === false) {
                          setModalDoneTaskVisible(true);
                        }

                        setDoneTask(!doneTask);
                        realm.write(() => {
                          realm.create(
                            'Task',
                            {id: item.id, done: !item.done},
                            'modified',
                          );
                        });
                        const data = realm
                          .objects('Task')
                          .filtered(
                            `soundDay == ${props.day} AND soundMonth == ${props.month} AND soundYear == ${props.year}`,
                          );

                        setUserTasks(data);
                      }}
                      leftContent={
                        <TouchableHighlight
                          style={{
                            backgroundColor: '#0B6DF6',
                            paddingVertical: paddingVerticalTask,
                            paddingHorizontal: paddingHorizontalTask,
                            borderRadius: 190,
                            marginTop: 20,
                            // marginRight: 15,
                            alignItems: 'flex-end',
                          }}>
                          <View
                            style={{
                              flexDirection: 'column',
                              alignItems: 'center',
                            }}>
                            <Ionicons
                              name="ios-checkmark-circle"
                              color="white"
                              size={iconTaskSize - 3}
                            />
                          </View>
                        </TouchableHighlight>
                      }>
                      <TouchableOpacity
                        onPress={() => {
                          setTaskToUpdate(item.id);
                          setInputNameTask(item.name);
                          setSelectedColor(item.color);
                          // setAlarm(item.alarm);
                          setTaskHour(item.soundHour);
                          setTaskMinute(item.soundMinute);
                          setSelectedIcon(item.icon);
                          setUserSubtasks(item.subtasks);
                          editTaskrefBottomModalTEST.current.open();
                        }}>
                        <View
                          style={{
                            backgroundColor: item.done ? '#EDEBEA' : item.color,
                            paddingVertical: paddingVerticalTask,
                            paddingHorizontal: paddingHorizontalTask,
                            borderRadius: 190,
                            marginTop: 20,
                            marginBottom: 10,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            width: '87%',
                            alignSelf: 'center',
                            alignItems: 'center',
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                            }}>
                            <MaterialCommunityIcons
                              name={item.icon}
                              size={iconTaskSize}
                              color="white"
                            />
                            <View
                              style={{
                                flexDirection: 'column',
                                justifyContent: 'center',
                                marginLeft: 10,
                              }}>
                              <Text
                                style={{
                                  ...styles.dayWeek,
                                  fontSize: nameTaskSize,
                                }}>
                                {item.subtasks.length > 0
                                  ? truncate(item.name, 22)
                                  : truncate(item.name, 30)}
                              </Text>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  alignItems: 'center',
                                  marginTop: 2,
                                }}>
                                <Ionicons
                                  name="notifications"
                                  color="white"
                                  size={modeTaskIconSize}
                                />
                                <Text
                                  style={{
                                    ...styles.dayWeek,
                                    fontSize: timeTaskSize,
                                  }}>
                                  {handleReadableDate(
                                    item.soundHour,
                                    item.soundMinute,
                                  )}
                                </Text>
                              </View>
                            </View>
                          </View>
                          {item.subtasks.length > 0 ? (
                            <Entypo
                              name="flow-cascade"
                              color="white"
                              size={iconListTaskSize}
                            />
                          ) : (
                            <Text />
                          )}
                        </View>
                      </TouchableOpacity>
                    </Swipeable>
                  )
                }
              />
            )}
          </View>
        </View>
        {createTaskModal()}
        {editTaskModalTEST()}
        {/* <Modal
          isVisible={modalDoneTaskVisible}
          animationIn="slideInLeft"
          animationOut="flash"
          swipeDirection="left"
          onSwipeComplete={() => setModalDoneTaskVisible(false)}
          onBackdropPress={() => setModalDoneTaskVisible(false)}>
          <View
            style={{
              backgroundColor: 'rgba(255, 255, 255, 0.9)',
              padding: 22,
              borderRadius: 25,
              width: '75%',
              height: '20%',
            }}>
            <CountdownCircleTimer
              isPlaying
              duration={10}
              colors={[
                ['#004777', 0.4],
                ['#F7B801', 0.4],
                ['#A30000', 0.2],
              ]}>
              {({remainingTime, animatedColor}) => (
                <Animated.Text style={{color: animatedColor}}>
                  {remainingTime}
                </Animated.Text>
              )}
            </CountdownCircleTimer>
          </View>
        </Modal> */}
        <Modal
          isVisible={isMenuModalVisible}
          animationIn="fadeIn"
          animationOut="fadeOut"
          animationInTiming={200}
          animationOutTiming={200}
          backdropTransitionInTiming={200}
          backdropTransitionOutTiming={200}
          swipeDirection="down"
          onSwipeComplete={() => setMenuModalVisible(false)}
          onBackdropPress={() => setMenuModalVisible(false)}>
          <View
            style={{
              backgroundColor: 'rgba(255, 255, 255, 0.9)',
              padding: 22,
              borderRadius: 25,
              width: '75%',
              height: '20%',
              alignSelf: 'center',
              alignItems: 'center',
            }}>
            {/* ACA TAMBIAN AGREGAR MAS CONFIGURACIONES COMO ELIMINAR TODOS LOS TASK O ELIMINAR LA RUTINA ETC */}
            <Text>{I18n.t('sortBy')}</Text>
            <SwitchSelector
              options={tasksSortSelector}
              initial={Number(selectedSort)}
              hasPadding
              borderColor="gray"
              selectedColor="black"
              textColor="white"
              buttonColor="white"
              buttonMargin={3}
              backgroundColor="black"
              fontSize={12}
              height={40}
              onPress={(value) => {
                storeSettingsData('sortSelected', value);
              }}
            />
          </View>
        </Modal>
      </View>
    );
  };

  return (
    <View>
      {userTasks <= 0 ? handleCreateTaskView() : handleShowTasksView()}
    </View>
  );
};

const styles = StyleSheet.create({
  conatiner: {
    marginTop: '6%',
    // marginTop: '15%', en la pregunta
    // alignItems: 'center',
    alignItems: 'center',
  },
  conatiner15: {
    // width: '91%',
    width: '100%',
  },
  conatiner2: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    // backgroundColor: 'blue',
    width: '85%',
    alignSelf: 'center',
  },
  conatiner3: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 2,
    // backgroundColor: 'green',
  },
  dayWeek: {
    color: 'white',
  },
  questionTxt: {
    fontSize: 25,
    color: '#FFFFFF',
  },
  addTxt: {
    textAlign: 'center',
    fontSize: 17,
    marginTop: 13,
    color: '#FFFFFF',
    marginBottom: 13,
  },
  btn: {
    backgroundColor: '#59EEFF',
    borderRadius: 150,
    paddingHorizontal: 7,
    paddingVertical: 6,
  },
  modalContainer: {
    backgroundColor: 'rgba(18, 18, 18, 0.8)',
    height: '100%',
    flexDirection: 'column-reverse',
  },
  modalContentView: {
    paddingTop: 25,
    paddingLeft: 35,
    paddingRight: 35,
  },
  modalTxt: {
    fontSize: 18,
    alignSelf: 'center',
    color: '#FFFFFF',
  },
  nameModalTxt: {
    fontSize: 16,
    marginBottom: 20,
    color: '#ffffff',
  },
  modalInput: {
    paddingVertical: 9,
    paddingHorizontal: 13,
    borderWidth: 1,
    backgroundColor: 'black',
    borderRadius: 8,
    marginBottom: 20,
    color: 'white',
    shadowColor: 'rgba(48, 48, 48, 10)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  modalBtnColorsTxt: {
    fontSize: 16,
    marginBottom: 20,
    color: '#ffffff',
  },
  timeTxt: {
    fontSize: 16,
    color: '#ffffff',
  },
  timeBtn: {
    marginTop: 20,
    backgroundColor: 'black',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 18,
    paddingRight: 18,
    borderRadius: 8,
    // shadowColor: 'rgba(25, 25, 25, 0.9)',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 0.5,
    shadowRadius: 5,

    elevation: 5,
    marginBottom: 20,
  },
  timeTxtAndSwitchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 25,
  },
  timeBtnTxt: {
    color: 'white',
  },
  switchContainer: {
    width: 135,
    marginLeft: 15,
  },
  iconsTxt: {
    fontSize: 16,
    marginBottom: 20,
    color: '#ffffff',
  },
  createContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 20,
    opacity: 1,
  },
  createBtn: {
    backgroundColor: 'rgba(31, 242, 251, 1)',
    paddingHorizontal: '8%',
    paddingVertical: '3%',
    borderRadius: 55,
  },
  createTxt: {
    fontSize: 16,
    color: 'white',
  },
});

export default Task;
