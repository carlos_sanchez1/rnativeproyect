import React from 'react';
import {View} from 'react-native';
import ModalX from 'react-native-modal';

const Modal = ({openModal, content, passCloseModal, style}) => {
  return (
    <ModalX
      isVisible={openModal}
      animationIn="fadeIn"
      animationOut="fadeOut"
      animationInTiming={100}
      animationOutTiming={400}
      backdropTransitionInTiming={600}
      backdropTransitionOutTiming={600}
      onBackdropPress={() => passCloseModal(false)}>
      <View style={style}>{content}</View>
    </ModalX>
  );
};

export default Modal;
