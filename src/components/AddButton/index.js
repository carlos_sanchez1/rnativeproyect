import React from 'react';

import Button from '../Button';

import AntDesign from 'react-native-vector-icons/AntDesign';

const AddButton = ({iconSize, onPress}) => {
  return (
    <>
      <Button
        onPress={onPress}
        content={
          <AntDesign name="pluscircle" size={iconSize} color="#59EEFF" />
        }
      />
    </>
  );
};

export default AddButton;
