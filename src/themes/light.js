import {DefaultTheme} from '@react-navigation/native';

export const lightTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: 'white',
    primary: 'black',
    text: 'black',
    otherDays: '#CDCDCD',
    modalWrapper: 'rgba(225, 225, 225, 0.7)',
    subModalWrapper: 'rgba(225, 225, 225, 0.6)',
    forms: '#EDEBEA',
    tabBar: '#222326',
  },
};
