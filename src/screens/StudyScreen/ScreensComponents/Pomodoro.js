import React, {useRef, useState, useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import LinearGradient from 'react-native-linear-gradient';
import {Picker} from '@react-native-picker/picker';

import StudyModuleContainer from '../../../components/StudyModulesContainer';
import AddButton from '../../../components/AddButton';
import BottomModal from '../../../components/BottomModal';
import TextModal from '../../../components/BottomModal/textModal';

import {useTheme} from '@react-navigation/native';

const Pomodoro = () => {
  const {colors} = useTheme();

  const [selectedLanguage, setSelectedLanguage] = useState();

  const newPomodoroRef = useRef();

  const newPomodoroModal = () => {
    return (
      <BottomModal
        openModal={newPomodoroRef}
        keyBoardPushContent={false}
        wrapperColor={colors.modalWrapper}
        muchContent={true}
        customSize={true}
        sizeModal={570}
        borderRadiusTop={40}
        closeDragDown={true}
        content={
          <View>
            <TextModal text="New Pomodoro" textTitle={true} />
            <Picker
              selectedValue={selectedLanguage}
              onValueChange={(itemValue, itemIndex) =>
                setSelectedLanguage(itemValue)
              }>
              <Picker.Item label="Java" value="java" />
              <Picker.Item label="JavaScript" value="js" />
            </Picker>
          </View>
        }
      />
    );
  };

  return (
    <View style={{alignItems: 'center', backgroundColor: null, height: '100%'}}>
      <StudyModuleContainer
        fixed={true}
        backgroundFigures={
          <LinearGradient
            colors={['#169587', '#A6CA63']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            style={{
              width: 250,
              height: 250,
              position: 'absolute',
              top: 150,
              left: 10,
              borderRadius: 200,
              transform: [{rotate: '180deg'}],
            }}
          />
        }
        leftContent={
          <>
            <Text
              style={{
                marginVertical: 40,
                color: 'white',
                fontWeight: 'bold',
                fontSize: 45,
              }}>
              Titulo
            </Text>
            <Text style={{color: 'white'}}>
              Estudia lo que quieras via notificacines con el metodo de
              repeticion constante
            </Text>
          </>
        }
        rightContentTop={
          <>
            <MaterialCommunityIcons
              name="head-flash"
              size={50}
              color="white"
              style={{
                transform: [{rotate: '15deg'}],
                left: 35,
              }}
            />
            <MaterialCommunityIcons
              name="progress-clock"
              size={70}
              color="white"
              style={{
                transform: [{rotate: '-15deg'}],
                backgroundColor: 'transparent',
                bottom: 20,
                right: 10,
              }}
            />
          </>
        }
        gradientColorsArray={['#169587', '#A6CA63']}
      />
      <View style={styles.bottomContainer}>
        <Text style={{marginBottom: 15, fontSize: 20}}>Add Pomodoro</Text>
        <AddButton
          onPress={() => newPomodoroRef.current.open()}
          iconSize={60}
        />
      </View>
      {newPomodoroModal()}
    </View>
  );
};

const styles = StyleSheet.create({
  bottomContainer: {
    backgroundColor: null,
    width: '100%',
    height: '40%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Pomodoro;
