import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import LinearGradient from 'react-native-linear-gradient';

import StudyModuleContainer from '../../../components/StudyModulesContainer';
import AddButton from '../../../components/AddButton';
const Exams = () => {
  return (
    <View style={{alignItems: 'center', backgroundColor: null, height: '100%'}}>
      <StudyModuleContainer
        fixed={true}
        backgroundFigures={
          <>
            <LinearGradient
              colors={['#791BF4', '#3880EC']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{
                width: 250,
                height: 250,
                position: 'absolute',
                top: 150,
                left: -100,
                borderRadius: 200,
                transform: [{rotate: '230deg'}],
              }}
            />
            <LinearGradient
              colors={['#791BF4', '#3880EC']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{
                width: 250,
                height: 250,
                position: 'absolute',
                bottom: 150,
                left: 230,
                borderRadius: 200,
                transform: [{rotate: '200deg'}],
              }}
            />
          </>
        }
        leftContent={
          <>
            <Text
              style={{
                marginVertical: 40,
                color: 'white',
                fontWeight: 'bold',
                fontSize: 45,
              }}>
              Titulo
            </Text>
            <Text style={{color: 'white'}}>
              Estudia lo que quieras via notificacines con el metodo de
              repeticion constante
            </Text>
          </>
        }
        rightContentTop={
          <>
            <Ionicons
              name="calendar"
              size={50}
              color="white"
              style={{
                transform: [{rotate: '15deg'}],
                left: 35,
              }}
            />
            <MaterialCommunityIcons
              name="file-clock"
              size={70}
              color="white"
              style={{
                transform: [{rotate: '-15deg'}],
                backgroundColor: 'transparent',
                bottom: 20,
                right: 10,
              }}
            />
          </>
        }
        gradientColorsArray={['#791BF4', '#3880EC']}
      />
      <View style={styles.bottomContainer}>
        <Text style={{marginBottom: 15, fontSize: 20}}>Add Exam</Text>
        <AddButton iconSize={60} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bottomContainer: {
    backgroundColor: null,
    width: '100%',
    height: '40%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Exams;
