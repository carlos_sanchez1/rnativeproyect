import React, {useEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Alert,
  FlatList,
  StyleSheet,
  Dimensions,
} from 'react-native';

import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';

import getRealm from '../../../../services/realm';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import AddButton from '../../../../components/AddButton';
import Button from '../../../../components/Button';
import BottomModal from '../../../../components/BottomModal';
import TextModal from '../../../../components/BottomModal/textModal';

import LinearGradient from 'react-native-linear-gradient';

import {courseColors} from '../../../../utils';

import {useTheme} from '@react-navigation/native';

const CourseFlashCards = ({route, navigation}) => {
  const {colors} = useTheme();
  const {courseTitle, color, courseId, courseFlashCardsArr} = route.params;

  const [flashCardFrontInput, setFlashCardFrontInput] = useState('');
  const [flashCardBackInput, setFlashCardBackInput] = useState('');

  useEffect(() => {
    navigation.setOptions({
      title: `${courseTitle}`,
    });
  }, [navigation, courseTitle]);

  const createFlashCardRef = useRef();

  const createFlashCardModal = () => {
    const handleCreateAndSeveNewFlashCard = async (front, back) => {
      const data = {
        id: uuidv4(),
        name: courseTitle,
        question: front,
        answer: back,
      };

      const realm = await getRealm();

      try {
        realm.write(() => {
          const foundCourseToAddFlashCard = realm.objectForPrimaryKey(
            'Course',
            courseId,
          );

          foundCourseToAddFlashCard.flashCards.push(data);

          createFlashCardRef.current.close();
        });
      } catch (error) {
        console.log('ERR', error);
      }
    };

    return (
      <BottomModal
        openModal={createFlashCardRef}
        keyBoardPushContent={false}
        wrapperColor={colors.modalWrapper}
        muchContent={true}
        customSize={true}
        sizeModal={650}
        borderRadiusTop={40}
        closeDragDown={true}
        content={
          <View
            style={{
              paddingHorizontal: 20,
              backgroundColor: null,
              height: '94%',
              justifyContent: 'space-between',
            }}>
            <View>
              <TextModal text="Study Card" textTitle={true} />
              <LinearGradient
                start={{x: 0.0, y: 0.25}}
                end={{x: 0.5, y: 1.0}}
                colors={[
                  courseColors[color].color1,
                  courseColors[color].color2,
                ]}
                style={{
                  marginVertical: 10,
                  backgroundColor: null,
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 50,
                  paddingVertical: 7,
                  paddingHorizontal: 18,
                }}>
                <Text style={{color: 'white'}}>{courseTitle}</Text>
              </LinearGradient>
              <TextModal text="Card Front" textTitle={false} />
              <TextInput
                autoFocus
                value={flashCardFrontInput}
                onChangeText={(value) => setFlashCardFrontInput(value)}
                placeholder="Ej. Titulo, Pregunta etc"
                //   enablesReturnKeyAutomatically
                //   onSubmitEditing={() =>
                //     createNotificationrefBottomModal.current.close()
                //   }
                //   onEndEditing={() =>
                //     createNotificationrefBottomModal.current.close()
                //   }
                style={{
                  backgroundColor: colors.forms,
                  paddingHorizontal: 25,
                  paddingVertical: 20,
                  borderRadius: 15,
                  marginVertical: 10,
                }}
              />

              <TextModal text="Card Back" textTitle={false} />
              <TextInput
                autoFocus
                value={flashCardBackInput}
                onChangeText={(value) => setFlashCardBackInput(value)}
                placeholder="Ej. Respuesta o significado de tu titlulo"
                //   enablesReturnKeyAutomatically
                //   onSubmitEditing={() =>
                //     createNotificationrefBottomModal.current.close()
                //   }
                // onEndEditing={() => addCourserefBottomModal.current.close()}
                style={{
                  backgroundColor: colors.forms,
                  paddingHorizontal: 25,
                  paddingTop: 20,
                  paddingBottom: 50,
                  borderRadius: 20,
                  marginVertical: 10,
                }}
              />
            </View>
            <View
              style={{
                backgroundColor: null,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              }}>
              <Button
                onPress={() => createFlashCardRef.current.close()}
                content={
                  <View
                    style={{
                      borderColor: 'blue',
                      borderWidth: 1,
                      paddingHorizontal: 45,
                      paddingVertical: 15,
                      borderRadius: 50,
                    }}>
                    <Text style={{color: 'blue'}}>Cancelar</Text>
                  </View>
                }
              />
              <Button
                onPress={() =>
                  flashCardFrontInput.length && flashCardBackInput.length > 0
                    ? handleCreateAndSeveNewFlashCard(
                        flashCardFrontInput,
                        flashCardBackInput,
                      )
                    : Alert.alert('debes llenar los campos')
                }
                content={
                  <View
                    style={{
                      backgroundColor: 'blue',
                      paddingHorizontal: 45,
                      paddingVertical: 15,
                      borderRadius: 50,
                    }}>
                    <Text style={{color: 'white'}}>Crear</Text>
                  </View>
                }
              />
            </View>
          </View>
        }
      />
    );
  };

  return (
    <View
      style={{
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      {courseFlashCardsArr.length > 0 ? (
        <View style={studyCardsStyles.flatlistFlashCardsContainer}>
          <FlatList
            data={courseFlashCardsArr}
            keyExtractor={(item) => item.id}
            // style={{backgroundColor: 'green'}}
            numColumns={1}
            renderItem={({item}) => (
              <Button
                onPress={() =>
                  navigation.navigate('flashCard', {
                    flashCardFront: item.question,
                    flashCardBack: item.answer,
                  })
                }
                content={
                  <Text style={{textAlign: 'center', color: colors.text}}>
                    {item.question}
                  </Text>
                }
                styleBtn={{
                  ...studyCardsStyles.flashCardItem,
                  backgroundColor: colors.forms,
                }}
              />
            )}
          />
          <View style={studyCardsStyles.floatingButtonContainer}>
            <AddButton
              onPress={() => createFlashCardRef.current.open()}
              iconSize={60}
            />
          </View>
        </View>
      ) : (
        <View style={{alignItems: 'center'}}>
          <Text>Agregar Card</Text>
          <AddButton
            onPress={() => createFlashCardRef.current.open()}
            iconSize={60}
          />
        </View>
      )}
      {createFlashCardModal()}
    </View>
  );
};

const studyCardsStyles = StyleSheet.create({
  flatlistFlashCardsContainer: {
    backgroundColor: null,
    width: '100%',
    height: '100%',
  },
  flashCardItem: {
    width: '90%',
    paddingVertical: 40,
    justifyContent: 'center',
    borderRadius: 20,
    alignSelf: 'center',
    marginTop: 25,
  },
  floatingButtonContainer: {
    position: 'absolute',
    left: '80%',
    top: '90%',
    backgroundColor: null,
  },
});

export default CourseFlashCards;
