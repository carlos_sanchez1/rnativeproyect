import React, {useEffect, useRef, useState} from 'react';
import {View, Text, TextInput, FlatList, Alert} from 'react-native';

import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';

import getRealm from '../../../../services/realm';

import AddButton from '../../../../components/AddButton';
import BottomModal from '../../../../components/BottomModal';
import Button from '../../../../components/Button';

import Ionicons from 'react-native-vector-icons/Ionicons';

import LinearGradient from 'react-native-linear-gradient';

import {useTheme} from '@react-navigation/native';

import {courseColors} from '../../../../utils';

import PushNotificationIOS from '@react-native-community/push-notification-ios';
import {
  handleScheduleLocalNotification,
  showNotification,
} from '../../../../notification';

const CourseNotifications = ({route, navigation}) => {
  const {colors} = useTheme();

  const {courseTitle, color, courseId, courseNotificationsArr} = route.params;
  useEffect(() => {
    navigation.setOptions({
      title: `${courseTitle}`,
    });
  }, [navigation, courseTitle, courseNotificationsArr, courseNotifications]);

  const [permissions, setPermissions] = useState({});

  useEffect(() => {
    PushNotificationIOS.addEventListener('notification', onRemoteNotification);
  }, []);

  const onRemoteNotification = (notification) => {
    const isClicked = notification.getData().userInteraction === 1;

    if (isClicked) {
      // Navigate user to another screen
      console.log('clisk');
    } else {
      // Do something else with push notification
      console.log('nel');
    }
  };

  const [notificationStudyTitle, setNotificationStudyTitle] = useState('');
  const [notificationStudyBody, setNotificationStudyBody] = useState('');

  const [courseNotifications, setCourseNotifications] = useState(false);

  const createNotificationrefBottomModal = useRef();

  const createNotificationModal = () => {
    const handleCreateAndSeveNewNotification = async function (
      notificationtitle,
      body,
    ) {
      const realm = await getRealm();

      try {
        realm.write(() => {
          const courseToAddNotification = realm.objectForPrimaryKey(
            'Course',
            courseId,
          );
          courseToAddNotification.notificationsStudy.push({
            id: uuidv4(),
            title: notificationtitle,
            content: body,
          });
        });
        setCourseNotifications(!courseNotifications);
        createNotificationrefBottomModal.current.close();
      } catch (error) {
        console.log('ERR', error);
      }
    };

    return (
      <BottomModal
        openModal={createNotificationrefBottomModal}
        keyBoardPushContent={false}
        wrapperColor={colors.modalWrapper}
        muchContent={true}
        customSize={true}
        sizeModal={570}
        borderRadiusTop={40}
        closeDragDown={true}
        content={
          <View
            style={{
              paddingHorizontal: 20,
              backgroundColor: null,
              height: '94%',
              justifyContent: 'space-between',
            }}>
            <View>
              <Text style={{alignSelf: 'center'}}>New Notification</Text>
              <LinearGradient
                start={{x: 0.0, y: 0.25}}
                end={{x: 0.5, y: 1.0}}
                colors={[
                  courseColors[color].color1,
                  courseColors[color].color2,
                ]}
                style={{
                  marginVertical: 10,
                  backgroundColor: null,
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 50,
                  paddingVertical: 7,
                  paddingHorizontal: 18,
                }}>
                <Text style={{color: 'white'}}>{courseTitle}</Text>
              </LinearGradient>
              <TextInput
                //   autoFocus
                value={notificationStudyTitle}
                onChangeText={(value) => setNotificationStudyTitle(value)}
                placeholder="Ej. ¿Cuando fue x Acontecimiento?, significado de x palabra, estudia lo que quieras"
                //   enablesReturnKeyAutomatically
                //   onSubmitEditing={() =>
                //     createNotificationrefBottomModal.current.close()
                //   }
                //   onEndEditing={() =>
                //     createNotificationrefBottomModal.current.close()
                //   }
                style={{
                  backgroundColor: colors.forms,
                  paddingHorizontal: 25,
                  paddingVertical: 20,
                  borderRadius: 15,
                  marginVertical: 10,
                }}
              />
              <TextInput
                //   autoFocus
                value={notificationStudyBody}
                onChangeText={(value) => setNotificationStudyBody(value)}
                placeholder="Ej. Respuesta o significado de tu titlulo"
                //   enablesReturnKeyAutomatically
                //   onSubmitEditing={() =>
                //     createNotificationrefBottomModal.current.close()
                //   }
                // onEndEditing={() => addCourserefBottomModal.current.close()}
                style={{
                  backgroundColor: colors.forms,
                  paddingHorizontal: 25,
                  paddingTop: 20,
                  paddingBottom: 50,
                  borderRadius: 20,
                  marginVertical: 10,
                }}
              />
            </View>
            <View
              style={{
                backgroundColor: null,
                flexDirection: 'row',
                justifyContent: 'space-evenly',
              }}>
              <Button
                onPress={() => createNotificationrefBottomModal.current.close()}
                content={
                  <View
                    style={{
                      borderColor: 'blue',
                      borderWidth: 1,
                      paddingHorizontal: 45,
                      paddingVertical: 15,
                      borderRadius: 50,
                    }}>
                    <Text style={{color: 'blue'}}>Cancelar</Text>
                  </View>
                }
              />
              <Button
                onPress={() =>
                  notificationStudyTitle.length === 0 &&
                  notificationStudyBody.length === 0
                    ? Alert.alert('Rellena los campos')
                    : handleCreateAndSeveNewNotification(
                        notificationStudyTitle,
                        notificationStudyBody,
                      )
                }
                content={
                  <View
                    style={{
                      backgroundColor: 'blue',
                      paddingHorizontal: 45,
                      paddingVertical: 15,
                      borderRadius: 50,
                    }}>
                    <Text style={{color: 'white'}}>Crear</Text>
                  </View>
                }
              />
            </View>
          </View>
        }
      />
    );
  };

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        backgroundColor: 'blue',
      }}>
      {courseNotificationsArr.length > 0 ? (
        <View style={{width: '100%', height: '100%'}}>
          <FlatList
            data={courseNotificationsArr}
            keyExtractor={(item) => item.id}
            style={{backgroundColor: 'green'}}
            numColumns={1}
            renderItem={({item}) => (
              <Button
                onPress={() => {}}
                content={
                  <View
                    style={{
                      backgroundColor: 'gray',
                      alignItems: 'center',
                    }}>
                    <Text>{item.title}</Text>
                    {/* item.sound === true y cuando toque la noti cambiar a false */}
                    {item.title === 'Juice'
                      ? handleScheduleLocalNotification(
                          'Water',
                          'agua',
                          2021,
                          2,
                          12,
                          19,
                          59,
                          0,
                        )
                      : null}
                  </View>
                }
                styleBtn={{
                  backgroundColor: 'red',
                  marginVertical: 15,
                  marginHorizontal: 20,
                  alignItems: 'center',
                  paddingVertical: 20,
                  borderRadius: 10,
                }}
              />
            )}
          />
          <View
            style={{
              position: 'absolute',
              left: '80%',
              top: '90%',
              backgroundColor: 'red',
            }}>
            <AddButton
              onPress={() => createNotificationrefBottomModal.current.open()}
              iconSize={60}
            />
          </View>
        </View>
      ) : (
        <View
          style={{
            backgroundColor: 'red',
            alignItems: 'center',
          }}>
          <Text>No tienes notificacines crea una</Text>
          <Text style={{marginBottom: 20, marginTop: 8}}>crear</Text>
          <AddButton
            onPress={() => createNotificationrefBottomModal.current.open()}
            iconSize={55}
          />
        </View>
      )}
      {createNotificationModal()}
    </View>
  );
};

export default CourseNotifications;
