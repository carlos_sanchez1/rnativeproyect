/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, TouchableOpacity} from 'react-native';
import {
  DrawerItem,
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import {useTheme, useNavigation} from '@react-navigation/native';

import AntDesign from 'react-native-vector-icons/AntDesign';

import I18n from '../../services/translation';
import getRealm from '../../services/realm';

import {responsive} from '../../utils';

const size = responsive();

export function CustomDrawerContent(props) {
  const {colors} = useTheme();
  const navigation = useNavigation();

  const [routines, setRoutines] = useState([]);

  const [starRoutine, setStartRoutine] = useState([]);

  useEffect(() => {
    const handleShowUserRoutines = async () => {
      try {
        const realm = await getRealm();
        setRoutines(realm.objects('Routine'));
      } catch (error) {
        console.log('ERRR', error);
      }
    };
    handleShowUserRoutines();
  }, []);

  const handleReadableDate = (hour, minute) => {
    var h = hour,
      m = minute;
    var time;
    if (h === 12) {
      time = m >= 0 && m <= 9 ? h + ':' + '0' + m + ' PM' : h + ':' + m + ' PM';
    } else {
      time =
        m >= 0 && m <= 9
          ? h > 12
            ? h - 12 + ':' + '0' + m + ' PM'
            : h + ':' + '0' + m + ' AM'
          : h > 12
          ? h - 12 + ':' + m + ' PM'
          : h + ':' + m + ' AM';
    }
    return time;
  };

  let drawerTitleFontSize;
  let newRoutineTextSize;
  let newRoutineIconSize;

  if (size === 'small') {
    drawerTitleFontSize = 12;
    newRoutineTextSize = 11;
    newRoutineIconSize = 15;
  } else if (size === 'medium') {
    drawerTitleFontSize = 14;
    newRoutineTextSize = 13;
    newRoutineIconSize = 17;
  } else {
    drawerTitleFontSize = 16;
    newRoutineTextSize = 15;
    newRoutineIconSize = 20;
  }

  return (
    <DrawerContentScrollView {...props}>
      <View style={{alignSelf: 'center'}}>
        <Text style={{color: colors.text, fontSize: drawerTitleFontSize}}>
          {I18n.t('yourRoutines')}
        </Text>
      </View>
      <TouchableOpacity onPress={() => navigation.navigate('NewRoutine')}>
        <View
          style={{
            marginTop: 9,
            flexDirection: 'row',
            padding: 10,
            alignItems: 'center',
            backgroundColor: colors.forms,
          }}>
          <Text
            style={{
              color: colors.text,
              fontSize: newRoutineTextSize,
              marginRight: 5,
            }}>
            {I18n.t('newRoutine')}
          </Text>
          <AntDesign
            name="pluscircleo"
            color={colors.text}
            size={newRoutineIconSize}
          />
        </View>
      </TouchableOpacity>
      {/* <DrawerItem
        label="Rutina Ejemplo"
        inactiveTintColor={colors.text}
        inactiveBackgroundColor={colors.forms}
      />
      <DrawerItem
        label="FIN DE SEMANA"
        inactiveTintColor={colors.text}
        inactiveBackgroundColor={colors.forms}
      /> */}
      <View style={{padding: 5}}>
        <FlatList
          data={routines}
          numColumns={1}
          keyExtractor={(item) => item.id}
          renderItem={({item}) => (
            // <DrawerItem
            //   label={item.name}
            //   inactiveTintColor={colors.text}
            //   inactiveBackgroundColor={colors.forms}
            // />
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('Routine', {
                  routineId: item.id,
                  nameRoutine: item.name,
                  tasksRoutineArr: item.tasks,
                })
              }
              style={{
                backgroundColor: colors.forms,
                marginVertical: 6,
                marginHorizontal: 8,
                paddingHorizontal: 9,
                paddingVertical: 13,
                borderRadius: 5,
              }}>
              <Text style={{color: colors.text}}>{item.name}</Text>
              {/* <Text style={{color: colors.text}}>
                {item.tasks.map((item2) =>
                  handleReadableDate(item2.soundHour, item2.soundMinute),
                )}
              </Text>
              <Text style={{fontSize: 12, color: colors.text}}>
                Empieza a las
              </Text> */}
            </TouchableOpacity>
          )}
        />
      </View>
    </DrawerContentScrollView>
  );
}
