import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Alert,
  ScrollView,
} from 'react-native';

import {useTheme} from '@react-navigation/native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import SettingsContainer from '../../../components/SettingsContainer';

import {getSettingsData} from '../../../utils';

const Account = () => {
  const {colors} = useTheme();

  const [userName, setUserName] = useState('Carlos V');
  const [email, setEmail] = useState('');

  // const getSettingsData = async (storageKey) => {
  //   try {
  //     const foundValue = await AsyncStorage.getItem(storageKey);
  //     console.log('valor:', foundValue);
  //     setEmail(foundValue);
  //   } catch (error) {
  //     console.log('ERR', error);
  //   }
  // };

  //   const removeSettingsData = async (storageKey) => {
  //     try {
  //       await AsyncStorage.removeItem(storageKey);
  //     } catch (error) {
  //       console.log('ERR', error);
  //     }
  //   };

  useEffect(() => {
    getSettingsData('email', (value) => {
      setEmail(value);
    });
  }, []);

  return (
    <ScrollView>
      <View style={styles.container}>
        <View>
          <MaterialCommunityIcons
            name="account-circle-outline"
            size={90}
            color="#B0B0B0"
          />
        </View>
        <View
          style={{
            //   backgroundColor: 'green',
            alignItems: 'center',
            width: '100%',
          }}>
          <Text
            style={{alignSelf: 'flex-start', marginLeft: 19, color: '#B0B0B0'}}>
            Name
          </Text>
          <SettingsContainer
            disablePress={false}
            textInput={true}
            inputVerticalPadding={15}
            inputHorizontalPadding={25}
            mainContent={
              <TextInput
                value={userName}
                onChangeText={(value) => setUserName(value)}
                style={{height: 25, backgroundColor: 'transparent'}}
              />
            }
            customMargin={true}
            marginTop={5}
            marginBottom={30}
          />
          <Text
            style={{alignSelf: 'flex-start', marginLeft: 19, color: '#B0B0B0'}}>
            Email
          </Text>
          <SettingsContainer
            disablePress={true}
            inputVerticalPadding={15}
            inputHorizontalPadding={25}
            mainContent={<Text>{email}</Text>}
            customMargin={true}
            marginTop={5}
            marginBottom={30}
          />
        </View>
        {/* <SettingsContainer
          leftContent={<Entypo name="log-out" color={colors.text} size={18} />}
          mainContent={<Text>Log Out</Text>}
          onPress={() => removeSettingsData('userToken')}
        /> */}
        <SettingsContainer
          leftContent={
            <MaterialCommunityIcons
              name="delete-alert-outline"
              color={colors.text}
              size={20}
            />
          }
          mainContent={<Text>Delete Account</Text>}
          onPress={() => Alert.alert('you sure?')}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    // backgroundColor: 'red',
    height: '100%',
    paddingVertical: 80,
  },
});

export default Account;
