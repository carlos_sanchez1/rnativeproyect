/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  View,
  Text,
  AppState,
  ScrollView,
  Switch,
  Linking,
  Platform,
  TextInput,
  Alert,
  ActivityIndicator,
  StatusBar,
  FlatList,
} from 'react-native';

//¡¡TODOOOO!!!! REFACTORIZAR FUNCIONES Y COMPONENTES FUNCIONALES EN SUS PROPIOS ARCHIVOS Y ASI CON GO ETC
import I18n from '../../../services/translation';

import {checkNotifications, openSettings} from 'react-native-permissions';

import LinearGradient from 'react-native-linear-gradient';
import MaskedView from '@react-native-community/masked-view';

import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {useTheme} from '@react-navigation/native';

import Button from '../../../components/Button';
import BottomModal from '../../../components/BottomModal';
import SettingsContainer from '../../../components/SettingsContainer';

import SettingsOptionsContext from '../../../contexts/SettingsOptionsContext';

import {
  storeSettingsData,
  getSettingsData,
  removeSettingsData,
} from '../../../utils';
import PushNotification from 'react-native-push-notification';

const SettingsContent = ({navigation}) => {
  const {colors} = useTheme();

  const [notifications, setNotifications] = useState(null);

  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);

  const [reload, setReload] = useState(false);

  const _handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
      setReload(true);
    }

    appState.current = nextAppState;
    setAppStateVisible(appState.current);
    console.log('AppState', appState.current);
    setReload(false);
  };

  // if (AppState.currentState === 'active') {
  //   PushNotification.localNotificationSchedule({
  //     id: 0,
  //     title: 'app State',
  //     message: 'ACTIVO',
  //     date: new Date(Date.now() + 2 * 1000),
  //     allowWhileIdle: true,
  //   });
  // } else if (AppState.currentState === 'background') {
  //   PushNotification.localNotificationSchedule({
  //     id: 0,
  //     title: 'app State',
  //     message: 'SEGUNDO PLANO',
  //     date: new Date(Date.now() + 2 * 1000),
  //     allowWhileIdle: true,
  //   });
  // } else if (AppState.currentState === 'inactive') {
  //   PushNotification.localNotificationSchedule({
  //     id: 0,
  //     title: 'app State',
  //     message: 'INACTIVO',
  //     date: new Date(Date.now() + 2 * 1000),
  //     allowWhileIdle: true,
  //   });
  // } else if (AppState.currentState === 'extension') {
  //   PushNotification.localNotificationSchedule({
  //     id: 0,
  //     title: 'app State',
  //     message: 'EXTENSION',
  //     date: new Date(Date.now() + 2 * 1000),
  //     allowWhileIdle: true,
  //   });
  // } else if (AppState.currentState === 'unknown') {
  //   PushNotification.localNotificationSchedule({
  //     id: 0,
  //     title: 'app State',
  //     message: 'DESCONOCIDO',
  //     date: new Date(Date.now() + 2 * 1000),
  //     allowWhileIdle: true,
  //   });
  // } else {
  //   PushNotification.localNotificationSchedule({
  //     id: 0,
  //     title: 'app State',
  //     message: 'NINGUNA DE LAS ANTERIORES',
  //     date: new Date(Date.now() + 2 * 1000),
  //     allowWhileIdle: true,
  //   });
  // }

  const [userName, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [userToken, setUserToken] = useState('');

  const [viewPassword, setViewPassword] = useState(false);

  const [auth, setAuth] = useState(null);
  const [authLoading, setAuthLoading] = useState(false);

  const handleAuth = () => {
    setAuthLoading(true);
    setAuth(false);
    storeSettingsData(
      'userToken',
      `er83vw023e${email.toUpperCase()}92czscq487${password.toUpperCase()}`,
    );
    storeSettingsData('email', `${email}`);
    setTimeout(() => {
      setAuthLoading(false);
      setAuth(true);
      setUserToken(true);
      loginrefBottomModal.current.close();
    }, 4000);
  };

  const handleOut = () => {
    setAuthLoading(true);

    setTimeout(() => {
      setAuthLoading(false);
      setAuth(false);
      setUserToken(false);
    }, 4000);
  };

  const gorefBottomModal = useRef();
  const loginrefBottomModal = useRef();
  const signinrefBottomModal = useRef();

  const goList = [
    {icon: 'alarm-check', text: 'Alarm fot tasks'},
    {icon: 'all-inclusive', text: 'Infinity'},
    {icon: 'bell-alert', text: 'Notis by importance'},
    {icon: 'bookmark-music', text: 'Sounds'},
    {icon: 'buffer', text: 'Stacks'},
    {icon: 'calendar-month', text: 'Remember'},
  ];

  const goModal = () => {
    return (
      <BottomModal
        openModal={gorefBottomModal}
        wrapperColor={colors.modalWrapper}
        muchContent={true}
        customSize={true}
        sizeModal={840}
        borderRadiusTop={10}
        keyBoardPushContent={false}
        closeDragDown={false}
        // customBackgroundColor={true}
        // background="black"
        content={
          <View style={{flexDirection: 'column'}}>
            <StatusBar barStyle="light-content" />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                // backgroundColor: 'red',
                padding: 15,
              }}>
              <AntDesign
                onPress={() => gorefBottomModal.current.close()}
                name="close"
                color={colors.text}
                size={30}
              />
            </View>

            <View
              style={{
                // backgroundColor: 'blue',
                flexDirection: 'column',
                height: '90%',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  // backgroundColor: 'pink',
                  alignItems: 'center',
                  marginBottom: 30,
                }}>
                <View
                  style={{
                    // backgroundColor: 'green',
                    flexDirection: 'row',
                    alignItems: 'center',
                    // justifyContent: 'center',
                  }}>
                  <Ionicons name="school" color={colors.text} size={38} />
                  <Text
                    style={{
                      fontSize: 40,
                      color: colors.text,
                      fontWeight: 'bold',
                    }}>
                    Skool
                  </Text>
                </View>

                <View
                  style={{
                    width: 50,
                    marginLeft: 150,
                    marginTop: -13,
                    // backgroundColor: 'red',
                  }}>
                  <MaskedView
                    style={{flexDirection: 'row', height: 35}}
                    maskElement={
                      <View
                        style={{
                          backgroundColor: 'transparent',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text style={{fontSize: 35, fontWeight: 'bold'}}>
                          GO
                        </Text>
                      </View>
                    }>
                    <LinearGradient
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 0}}
                      colors={['#FC314D', '#FC756C', '#E03BBA']}
                      style={{flex: 1}}
                    />
                  </MaskedView>
                </View>
              </View>

              <View style={{backgroundColor: 'transparent', marginBottom: 30}}>
                <FlatList
                  data={goList}
                  keyExtractor={(item) => item.icon}
                  numColumns={1}
                  scrollEnabled={false}
                  renderItem={({item}) => (
                    <View
                      style={{
                        paddingHorizontal: 50,
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginBottom: 25,
                      }}>
                      <View
                        style={{
                          width: 30,
                          alignItems: 'center',
                          // backgroundColor: 'yellow',
                          marginRight: 10,
                        }}>
                        <MaskedView
                          style={{flexDirection: 'row', height: 30}}
                          maskElement={
                            <View
                              style={{
                                backgroundColor: 'transparent',
                                justifyContent: 'center',
                                alignItems: 'center',
                                flexDirection: 'row',
                              }}>
                              <MaterialCommunityIcons
                                name={item.icon}
                                size={30}
                              />
                            </View>
                          }>
                          <LinearGradient
                            start={{x: 0.1, y: -0.1}}
                            end={{x: 1, y: 0}}
                            colors={['#FC314D', '#FC756C', '#E03BBA']}
                            style={{flex: 1}}
                          />
                        </MaskedView>
                      </View>
                      <Text style={{fontSize: 17, color: colors.text}}>
                        {item.text}
                      </Text>
                    </View>
                  )}
                />
              </View>

              <View style={{alignItems: 'center'}}>
                <Button
                  onPress={() => {}}
                  content={
                    <LinearGradient
                      start={{x: 0, y: 0}}
                      end={{x: 1, y: 0}}
                      colors={['#FC314D', '#FC756C', '#E03BBA']}
                      style={{
                        paddingHorizontal: 20,
                        paddingVertical: 30,
                        borderRadius: 50,
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontSize: 20,
                          fontWeight: 'bold',
                          textAlign: 'center',
                          color: '#ffffff',
                          backgroundColor: 'transparent',
                          shadowColor: 'white',
                          shadowOpacity: 2,
                          shadowRadius: 5,
                          shadowOffset: {
                            width: 0,
                            height: 1,
                          },
                        }}>
                        Prueba gratis
                      </Text>
                    </LinearGradient>
                  }
                  styleBtn={{
                    width: '85%',
                    marginVertical: 15,
                  }}
                />
              </View>
            </View>
          </View>
        }
      />
    );
  };

  const loginModal = () => {
    return (
      <BottomModal
        openModal={loginrefBottomModal}
        wrapperColor={colors.subModalWrapper}
        muchContent={true}
        customSize={true}
        sizeModal={840}
        borderRadiusTop={10}
        keyBoardPushContent={false}
        closeDragDown={false}
        content={
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                // backgroundColor: 'red',
                padding: 15,
              }}>
              <AntDesign
                onPress={() => loginrefBottomModal.current.close()}
                name="close"
                color={colors.text}
                size={30}
              />
            </View>
            <View
              style={{
                // backgroundColor: 'blue',
                height: '87%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View>
                <View
                  style={{
                    marginBottom: 50,
                    // backgroundColor: 'pink',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Ionicons name="school" color={colors.text} size={45} />
                  <Text
                    style={{
                      fontSize: 38,
                      marginLeft: 4,
                      fontWeight: 'bold',
                      color: colors.text,
                    }}>
                    Skool
                  </Text>
                </View>

                <View>
                  <TextInput
                    value={email}
                    onChangeText={(value) => setEmail(value)}
                    placeholder="Email"
                    placeholderTextColor="#ADADAF"
                    style={{
                      backgroundColor: colors.forms,
                      color: colors.text,
                      paddingVertical: 15,
                      paddingHorizontal: 25,
                      width: 370,
                      height: 65,
                      borderRadius: 10,
                      marginVertical: 10,
                    }}
                  />
                  <TextInput
                    value={password}
                    onChangeText={(value) => setPassword(value)}
                    placeholder="Password"
                    placeholderTextColor="#ADADAF"
                    secureTextEntry={viewPassword ? false : true}
                    style={{
                      backgroundColor: colors.forms,
                      color: colors.text,
                      paddingVertical: 15,
                      paddingHorizontal: 25,
                      width: 370,
                      height: 65,
                      borderRadius: 10,
                      marginVertical: 10,
                    }}
                  />

                  {viewPassword ? (
                    <Ionicons
                      name="eye-off-outline"
                      color="#ADADAF"
                      size={15}
                      style={{bottom: 50, left: 330}}
                      onPress={() => setViewPassword(!viewPassword)}
                    />
                  ) : (
                    <Ionicons
                      name="eye-outline"
                      color="#ADADAF"
                      size={15}
                      style={{bottom: 50, left: 330}}
                      onPress={() => setViewPassword(!viewPassword)}
                    />
                  )}

                  <Button
                    onPress={() => {}}
                    content={
                      <Text style={{alignSelf: 'flex-end', color: colors.text}}>
                        Forgot Password ?
                      </Text>
                    }
                  />

                  <Button
                    onPress={() => {
                      email.length && password.length > 0
                        ? handleAuth()
                        : Alert.alert(
                            'Introduce un Correo y Contraseña Validos',
                          );
                    }}
                    content={
                      auth ? (
                        <Ionicons
                          name="checkmark-outline"
                          color="white"
                          size={30}
                        />
                      ) : authLoading ? (
                        <ActivityIndicator size="small" />
                      ) : (
                        <Text>Log in</Text>
                      )
                    }
                    styleBtn={{
                      backgroundColor: 'lightblue',
                      alignItems: 'center',
                      alignSelf: 'center',
                      paddingVertical: 20,
                      borderRadius: 10,
                      width: '95%',
                      marginTop: 30,
                    }}
                  />

                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      // backgroundColor: 'yellow',
                      marginTop: 40,
                    }}>
                    <Text style={{color: colors.text, marginRight: 4}}>
                      Don't have an account?
                    </Text>
                    <Button
                      onPress={() => {
                        signinrefBottomModal.current.open();
                      }}
                      content={<Text style={{color: 'purple'}}>Sign Up</Text>}
                    />
                  </View>
                </View>
              </View>
            </View>
            {signinModal()}
          </View>
        }
      />
    );
  };

  const signinModal = () => {
    return (
      <BottomModal
        openModal={signinrefBottomModal}
        wrapperColor={colors.subModalWrapper}
        muchContent={true}
        customSize={true}
        sizeModal={840}
        borderRadiusTop={10}
        keyBoardPushContent={false}
        closeDragDown={false}
        content={
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                // backgroundColor: 'red',
                padding: 15,
              }}>
              <AntDesign
                onPress={() => signinrefBottomModal.current.close()}
                name="close"
                color={colors.text}
                size={30}
              />
            </View>
            <View
              style={{
                // backgroundColor: 'blue',
                height: '87%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View>
                <View
                  style={{
                    marginBottom: 50,
                    // backgroundColor: 'pink',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Ionicons name="school" color={colors.text} size={45} />
                  <Text
                    style={{fontSize: 38, marginLeft: 4, fontWeight: 'bold'}}>
                    Skool
                  </Text>
                </View>

                <View>
                  <TextInput
                    value={userName}
                    onChangeText={(value) => setUserName(value)}
                    placeholder="Name"
                    style={{
                      backgroundColor: colors.forms,
                      paddingVertical: 15,
                      paddingHorizontal: 10,
                      width: 370,
                      height: 65,
                      borderRadius: 10,
                      marginVertical: 10,
                    }}
                  />
                  <TextInput
                    value={email}
                    onChangeText={(value) => setEmail(value)}
                    placeholder="Email"
                    style={{
                      backgroundColor: colors.forms,
                      paddingVertical: 15,
                      paddingHorizontal: 10,
                      width: 370,
                      height: 65,
                      borderRadius: 10,
                      marginVertical: 10,
                    }}
                  />
                  <TextInput
                    value={password}
                    onChangeText={(value) => setPassword(value)}
                    placeholder="Password"
                    secureTextEntry={viewPassword ? false : true}
                    style={{
                      backgroundColor: colors.forms,
                      paddingVertical: 15,
                      paddingHorizontal: 10,
                      width: 370,
                      height: 65,
                      borderRadius: 10,
                      marginVertical: 10,
                    }}
                  />

                  {viewPassword ? (
                    <Ionicons
                      name="eye-off-outline"
                      color="gray"
                      size={15}
                      style={{bottom: 50, left: 300}}
                      onPress={() => setViewPassword(!viewPassword)}
                    />
                  ) : (
                    <Ionicons
                      name="eye-outline"
                      color="gray"
                      size={15}
                      style={{bottom: 50, left: 300}}
                      onPress={() => setViewPassword(!viewPassword)}
                    />
                  )}

                  <Button
                    onPress={() => {}}
                    content={
                      <Text style={{alignSelf: 'flex-end'}}>
                        Forgot Password ?
                      </Text>
                    }
                  />

                  <Button
                    onPress={() => {
                      handleAuth();
                    }}
                    content={
                      auth ? (
                        <AntDesign name="checkcircle" color="white" size={30} />
                      ) : authLoading ? (
                        <ActivityIndicator size="small" />
                      ) : (
                        <Text>Sign in</Text>
                      )
                    }
                    styleBtn={{
                      backgroundColor: 'lightblue',
                      alignItems: 'center',
                      alignSelf: 'center',
                      paddingVertical: 20,
                      borderRadius: 10,
                      width: '95%',
                      marginTop: 30,
                    }}
                  />

                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      // backgroundColor: 'yellow',
                      marginTop: 40,
                    }}>
                    <Text style={{marginRight: 4}}>Have an account?</Text>
                    <Button
                      onPress={() => signinrefBottomModal.current.close()}
                      content={<Text style={{color: 'purple'}}>Sign In</Text>}
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
        }
      />
    );
  };

  const {issues, setIssues, dark, setDark} = useContext(SettingsOptionsContext);

  const [switchTheme, setSwitchTheme] = useState(null);

  useEffect(() => {
    userToken ? getSettingsData('userToken') : console.log('CERRASTE SESION');
  }, [userToken]);

  useEffect(() => {
    getSettingsData('darkTheme', (value) => {
      console.log('el dark', value);
      if (value === 'true') {
        setSwitchTheme(true);
        setDark(true);
      } else {
        setSwitchTheme(false);
        setDark(false);
      }
    });
  }, [dark, setDark]);

  useEffect(() => {
    checkNotifications().then(({status, settings}) => {
      if (status === 'granted') {
        setNotifications(true);
      } else {
        setNotifications(false);
      }
    });
    AppState.addEventListener('change', _handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  }, [reload, issues, setIssues]);

  useEffect(() => {
    const handleIssues = () => {
      if (notifications && auth) {
        setIssues(false);
        removeSettingsData('issues'); //esto para el async de notis
      } else {
        setIssues(true);
        storeSettingsData('issues', 'true');
      }
    };
    handleIssues();
  }, [notifications, auth, setIssues]);

  return (
    <ScrollView style={{flex: 1}}>
      <View style={{alignItems: 'center'}}>
        <Button
          onPress={() => gorefBottomModal.current.open()}
          content={
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#FB2047', '#FF7A65', '#DC34C3']}
              style={{
                paddingHorizontal: 20,
                paddingVertical: 25,
                borderRadius: 20,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Ionicons name="school" color="white" size={15} />
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: 'Gill Sans',
                  fontWeight: 'bold',
                  textAlign: 'center',
                  color: '#ffffff',
                  backgroundColor: 'transparent',
                }}>
                GO
              </Text>
            </LinearGradient>
          }
          styleBtn={{
            width: '92%',
            marginVertical: 15,
          }}
        />
        {goModal()}

        <SettingsContainer
          leftContent={<AntDesign name="user" color={colors.text} size={20} />}
          mainContent={
            auth ? (
              <Text style={{color: colors.text}}>My Account</Text>
            ) : (
              <Text style={{color: colors.text}}>{I18n.t('login')}</Text>
            )
          }
          rightContent={
            auth ? (
              <Entypo
                name="chevron-small-right"
                color={colors.text}
                size={20}
              />
            ) : (
              <MaterialIcons name="error" color="red" size={20} />
            )
          }
          onPress={() => {
            if (auth) {
              navigation.navigate('Account');
            } else {
              setEmail('');
              setPassword('');
              loginrefBottomModal.current.open();
            }
          }}
        />
        {loginModal()}

        <SettingsContainer
          leftContent={
            <MaterialIcons name="bar-chart" color={colors.text} size={20} />
          }
          mainContent={<Text style={{color: colors.text}}>Your Advance</Text>}
          rightContent={
            <Entypo name="chevron-small-right" color={colors.text} size={20} />
          }
          onPress={() => Alert.alert('advance')}
        />

        <SettingsContainer
          mainContent={
            notifications ? (
              <Text style={{color: colors.text}}>Notifications settings</Text>
            ) : (
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={{color: colors.text}}>Activate Notifications</Text>
                <MaterialIcons name="error" color="red" size={20} />
              </View>
            )
          }
          rightContent={
            notifications ? (
              <Entypo
                name="chevron-small-right"
                color={colors.text}
                size={20}
              />
            ) : (
              <FontAwesome name="external-link" color={colors.text} size={18} />
            )
          }
          onPress={() =>
            notifications ? Alert.alert('settings') : openSettings()
          }
        />

        <SettingsContainer
          mainContent={<Text style={{color: colors.text}}>Dark Theme</Text>}
          rightContent={
            <Switch
              value={switchTheme}
              onValueChange={(value) => {
                setDark(!dark);
                value
                  ? storeSettingsData('darkTheme', 'true')
                  : removeSettingsData('darkTheme');
              }}
            />
          }
          disablePress={true}
        />

        <SettingsContainer
          mainContent={<Text style={{color: colors.text}}>Tasks Settings</Text>}
          rightContent={
            <Entypo name="chevron-small-right" color={colors.text} size={20} />
          }
          onPress={() => navigation.navigate('Tasks Settings')}
        />

        <SettingsContainer
          leftContent={
            <FontAwesome name="star" color={colors.text} size={18} />
          }
          mainContent={
            Platform.OS === 'ios' ? (
              <Text style={{color: colors.text}}>Rate us On App Store</Text>
            ) : (
              <Text style={{color: colors.text}}>Rate us On PlayStore</Text>
            )
          }
          rightContent={
            <FontAwesome name="external-link" color={colors.text} size={18} />
          }
          onPress={() =>
            Linking.openURL('https://reactnative.dev/docs/linking')
          }
          settingsGroup={true}
          borderRadiusTop={10}
          customMargin={true}
          marginTop={20}
          marginBottom={0}
        />

        <SettingsContainer
          leftContent={
            <FontAwesome name="instagram" color={colors.text} size={20} />
          }
          mainContent={<Text style={{color: colors.text}}>Instagram</Text>}
          rightContent={
            <FontAwesome name="external-link" color={colors.text} size={18} />
          }
          onPress={() => Alert.alert('insta')}
          settingsGroup={true}
          customMargin={true}
          marginTop={0}
          marginBottom={0}
        />

        <SettingsContainer
          leftContent={
            <FontAwesome name="twitter" color={colors.text} size={20} />
          }
          mainContent={<Text style={{color: colors.text}}>Twitter</Text>}
          rightContent={
            <FontAwesome name="external-link" color={colors.text} size={18} />
          }
          onPress={() => Alert.alert('twt')}
          settingsGroup={true}
          customMargin={true}
          marginTop={0}
          marginBottom={0}
        />

        <SettingsContainer
          leftContent={
            <FontAwesome
              name="facebook-official"
              color={colors.text}
              size={20}
            />
          }
          mainContent={<Text style={{color: colors.text}}>Facebook</Text>}
          rightContent={
            <FontAwesome name="external-link" color={colors.text} size={18} />
          }
          onPress={() => Alert.alert('fb')}
          settingsGroup={true}
          borderRadiusBottom={10}
          customMargin={true}
          marginTop={0}
          marginBottom={20}
        />

        <SettingsContainer
          leftContent={
            <Ionicons
              name="help-circle-outline"
              color={colors.text}
              size={20}
            />
          }
          mainContent={<Text style={{color: colors.text}}>Help</Text>}
          rightContent={
            <Entypo name="chevron-small-right" color={colors.text} size={20} />
          }
          onPress={() => Alert.alert('help')}
        />

        <SettingsContainer
          leftContent={
            <FontAwesome name="trash-o" color={colors.text} size={18} />
          }
          mainContent={<Text style={{color: colors.text}}>Trash</Text>}
          rightContent={
            <Entypo name="chevron-small-right" color={colors.text} size={20} />
          }
          onPress={() => Alert.alert('trash')}
        />

        <SettingsContainer
          leftContent={
            <MaterialIcons name="privacy-tip" color={colors.text} size={20} />
          }
          mainContent={<Text style={{color: colors.text}}>Privacy</Text>}
          rightContent={
            <Entypo name="chevron-small-right" color={colors.text} size={20} />
          }
          onPress={() => Alert.alert('ols')}
        />

        {auth ? (
          <SettingsContainer
            leftContent={
              <Entypo name="log-out" color={colors.text} size={18} />
            }
            mainContent={
              auth ? (
                authLoading ? (
                  <ActivityIndicator size="small" />
                ) : (
                  <Text style={{color: colors.text}}>Log Out</Text>
                )
              ) : (
                <Ionicons name="checkmark-outline" color="white" size={30} />
              )
            }
            onPress={handleOut}
            customMargin={true}
            marginTop={40}
            marginBottom={15}
          />
        ) : null}
      </View>
    </ScrollView>
  );
};

export default SettingsContent;
