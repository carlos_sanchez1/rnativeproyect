import Realm from 'realm';

import {TaskSchema, SubtaskSchema} from '../schemas/TaskSchema';
import {RoutineSchema} from '../schemas/RoutineSchema';
import {
  CourseSchema,
  FlashCardSchema,
  NotificationStudySchema,
} from '../schemas/CourseSchema';

export default function getRealm() {
  return Realm.open({
    schema: [
      TaskSchema,
      RoutineSchema,
      SubtaskSchema,
      CourseSchema,
      FlashCardSchema,
      NotificationStudySchema,
    ],
    schemaVersion: 1,
  });
}
