const CourseSchema = {
  name: 'Course',
  primaryKey: 'id',
  properties: {
    id: 'string',
    name: 'string',
    color: 'string',
    icon: 'string',
    flashCards: {type: 'list', objectType: 'flashCard'},
    notificationsStudy: {type: 'list', objectType: 'notificationStudy'},
  },
};

const FlashCardSchema = {
  name: 'flashCard',
  primaryKey: 'id',
  properties: {
    id: 'string',
    name: 'string',
    question: 'string',
    answer: 'string',
  },
};

const NotificationStudySchema = {
  name: 'notificationStudy',
  primaryKey: 'id',
  properties: {
    id: 'string',
    title: 'string',
    content: 'string',
  },
};

export {CourseSchema, FlashCardSchema, NotificationStudySchema};
