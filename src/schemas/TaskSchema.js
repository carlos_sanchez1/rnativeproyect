const TaskSchema = {
  name: 'Task',
  primaryKey: 'id',
  properties: {
    id: 'string',
    name: 'string',
    color: 'string',
    done: 'bool',
    icon: 'string',
    soundYear: 'int',
    soundMonth: 'int',
    soundDay: 'int',
    soundHour: 'int',
    soundMinute: 'int',
    subtasks: {type: 'list', objectType: 'Subtask'},
  },
};

const SubtaskSchema = {
  name: 'Subtask',
  primaryKey: 'id',
  properties: {
    id: 'string',
    name: 'string',
    done: 'bool',
  },
};

export {TaskSchema, SubtaskSchema};
