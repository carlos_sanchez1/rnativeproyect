const RoutineSchema = {
  name: 'Routine',
  primaryKey: 'id',
  properties: {
    id: 'string',
    name: 'string',
    tasks: {type: 'list', objectType: 'Task'},
  },
};

export {RoutineSchema};
